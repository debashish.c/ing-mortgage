# ING-Mortgage-NL

```
cd existing_repo
git remote add origin https://gitlab.com/debashish.c/ing-mortgage.git
git branch -M main
git push -uf origin main
```


**API Details**
* GET /api/interest-rates (get a list of current interest rates):
* http://localhost:8080/api/interest-rates
* POST /api/mortgage-check (post the parameters to calculate for a mortgage check):
* http://localhost:8080/api/mortgage-check
* Payload :
* {
  "income": 24990,
  "maturityPeriod": 5,
  "loanValue": 10000,
  "homeValue": 1000,
  "interestRate": 10
  }
* API Documentation :
* http://localhost:8080/swagger-ui/index.html

**Business Rules**

*  Mortgage should not exceed 4 times the income
*  Mortgage should not exceed the home value

**Technologies Used**
* SpringBoot v3.1
* Java 20
* Junit 
* Karate with Cucumber Reporting ( run TestRunner.java to generate the integration test report in target/cucumber-html-reports)

**Note**
I have also implemented the spring data jpa with h2 database in-case interest rates needs to be fetch from db instead of hardcode value in java file.
To run  we need to uncomment code from InterestRateController.java and run the below end point

@GetMapping("api/interest-rates-db")

@PostMapping("api/save/interest-rates")






**Test Report: ING Mortgage NL**

![img.png](img.png)
