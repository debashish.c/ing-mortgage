package com.ing.mortgage;

import com.ing.mortgage.model.InterestRate;
import com.ing.mortgage.model.MortgageCheckRequest;
import com.ing.mortgage.service.MortgageRateServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

/**
 * The MortgageRateServiceImplTest class contains test methods for the MortgageRateServiceImpl class.
 */
public class MortgageRateServiceImplTest {

    private MortgageRateServiceImpl mortgageRateService;

    /**
     * Sets up the test fixture by creating an instance of MortgageRateServiceImpl.
     */
    @BeforeEach
    public void setUp() {
        mortgageRateService = new MortgageRateServiceImpl();
    }

    /**
     * Tests the getCurrentInterestRates method of MortgageRateServiceImpl.
     * It verifies that the list of interest rates is not null and has the expected size.
     */
    @Test
    public void testGetCurrentInterestRates() {
        List<InterestRate> interestRates = mortgageRateService.getCurrentInterestRates();
        assertNotNull(interestRates);
        assertEquals(3, interestRates.size());
    }

    /**
     * Tests the isFeasible method of MortgageRateServiceImpl.
     * It verifies the feasibility of mortgage requests by checking if they meet the criteria.
     */
    @Test
    public void testIsFeasible() {
        MortgageCheckRequest feasibleRequest = new MortgageCheckRequest();
        feasibleRequest.setIncome(new BigDecimal("5000"));
        feasibleRequest.setLoanValue(new BigDecimal("10000"));
        feasibleRequest.setHomeValue(new BigDecimal("50000"));

        assertTrue(mortgageRateService.isFeasible(feasibleRequest));

        MortgageCheckRequest infeasibleRequest = new MortgageCheckRequest();
        infeasibleRequest.setIncome(new BigDecimal("5000"));
        infeasibleRequest.setLoanValue(new BigDecimal("50000"));
        infeasibleRequest.setHomeValue(new BigDecimal("30000"));

        assertFalse(mortgageRateService.isFeasible(infeasibleRequest));
    }

    /**
     * Tests the calculateMonthlyCosts method of MortgageRateServiceImpl.
     * It verifies the accuracy of the monthly costs calculation for a mortgage request.
     */
    @Test
    public void testCalculateMonthlyCosts() {
        MortgageCheckRequest request = new MortgageCheckRequest();
        request.setInterestRate(BigDecimal.valueOf(3.5));
        request.setMaturityPeriod(20);
        request.setLoanValue(BigDecimal.valueOf(200000));

        BigDecimal monthlyCosts = mortgageRateService.calculateMonthlyCosts(request);

        BigDecimal expectedMonthlyCosts = BigDecimal.valueOf(1159.92).setScale(2, RoundingMode.HALF_UP);
        assertEquals(expectedMonthlyCosts, monthlyCosts);
    }
}
