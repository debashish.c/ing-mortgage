package com.ing.mortgage;


import com.ing.mortgage.controller.InterestRateController;
import com.ing.mortgage.model.InterestRate;
import com.ing.mortgage.model.MortgageCheckRequest;
import com.ing.mortgage.model.MortgageCheckResponse;
import com.ing.mortgage.repository.InterestRateRepository;
import com.ing.mortgage.service.MortgageRateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(InterestRateController.class)
public class InterestRateControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MortgageRateService mortgageRateService;

    @BeforeEach
    public void setUp() {
        // Mock the behavior of mortgageRateService methods
        List<InterestRate> interestRates = new ArrayList<>();
        interestRates.add(new InterestRate(1, new BigDecimal("3.5"), LocalDateTime.now()));
        interestRates.add(new InterestRate(5, new BigDecimal("4.2"), LocalDateTime.now()));
        when(mortgageRateService.getCurrentInterestRates()).thenReturn(interestRates);

        when(mortgageRateService.isFeasible(any(MortgageCheckRequest.class))).thenReturn(true);
        when(mortgageRateService.calculateMonthlyCosts(any(MortgageCheckRequest.class))).thenReturn(new BigDecimal("1000"));
    }

    /**
     * Tests the retrieval of interest rates from the API endpoint.
     *
     * <p>The test scenario includes performing a GET request to the "/api/interest-rates" endpoint
     * and asserting the response status code, the length of the returned JSON array, and the values
     * of the "maturityPeriod" and "interestRate" fields of the interest rates.</p>
     *
     * @throws Exception if an error occurs during the test
     */
    @Test
    public void testGetInterestRates() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/interest-rates")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].maturityPeriod").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].interestRate").value(3.5))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].maturityPeriod").value(5))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].interestRate").value(4.2));
    }

    /**
     * Tests the mortgage check functionality using the API endpoint.
     *
     * <p>The test scenario includes performing a POST request to the "/api/mortgage-check" endpoint
     * with a sample MortgageCheckRequest JSON payload. It asserts the response status code, the value
     * of the "feasible" field in the returned MortgageCheckResponse, and the value of the "monthlyCosts"
     * field in the returned MortgageCheckResponse.</p>
     *
     * @throws Exception if an error occurs during the test
     */
    @Test
    public void testPerformMortgageCheck() throws Exception {
        MortgageCheckRequest request = new MortgageCheckRequest();
        request.setIncome(new BigDecimal("5000"));
        request.setMaturityPeriod(10);
        request.setLoanValue(new BigDecimal("200000"));
        request.setHomeValue(new BigDecimal("300000"));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/mortgage-check")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"income\": 5000, \"maturityPeriod\": 10, \"loanValue\": 200000, \"homeValue\": 300000}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.feasible").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.monthlyCosts").value(1000));
    }
    @MockBean
    private InterestRateRepository interestRateRepository;
    @Test
    public void testGetInterestRatesFromDB() throws Exception {
        // Create a list of interest rates to be returned by the repository
        List<InterestRate> interestRates = new ArrayList<>();
        interestRates.add(new InterestRate(1, new BigDecimal("2.5"), LocalDateTime.now()));
        interestRates.add(new InterestRate(5, new BigDecimal("3.2"), LocalDateTime.now()));

        // Mock the behavior of interestRateRepository.findAll()
        Mockito.when(interestRateRepository.findAll()).thenReturn(interestRates);

        // Perform the GET request to the endpoint
        mockMvc.perform(MockMvcRequestBuilders.get("/interest-rates-db")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].maturityPeriod").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].interestRate").value(2.5))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].maturityPeriod").value(5))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].interestRate").value(3.2));

        // Verify that interestRateRepository.findAll() was called
        Mockito.verify(interestRateRepository, Mockito.times(1)).findAll();
    }

}
