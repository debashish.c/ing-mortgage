package com.ing.mortgage.controller;

import com.ing.mortgage.model.InterestRate;
import com.ing.mortgage.model.MortgageCheckRequest;
import com.ing.mortgage.model.MortgageCheckResponse;
import com.ing.mortgage.repository.InterestRateRepository;
import com.ing.mortgage.service.MortgageRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
/**
 * The InterestRateController class is a REST controller that handles requests related to interest rates and mortgage checks.
 */
@RestController
@RequestMapping("/api")
public class InterestRateController {
    @Autowired
   MortgageRateService mortgageRateService;

    /**
     * Retrieves the list of interest rates.
     *
     * @return The list of interest rates.
     */
    @GetMapping("/interest-rates")
    public List<InterestRate> getInterestRates() {
      return mortgageRateService.getCurrentInterestRates();
    }

    /**
     * Performs a mortgage check based on the provided request.
     *
     * @param request The MortgageCheckRequest object containing the mortgage details.
     * @return The MortgageCheckResponse object containing the feasibility and monthly costs.
     */
    @PostMapping("/mortgage-check")
    public MortgageCheckResponse performMortgageCheck(@RequestBody MortgageCheckRequest request) {
        //check the mortgage feasibilty
        boolean feasible = mortgageRateService.isFeasible(request);
        // Calculate monthly costs
        BigDecimal monthlyCosts = mortgageRateService.calculateMonthlyCosts(request);
        return new MortgageCheckResponse(feasible, monthlyCosts);
    }
    private final InterestRateRepository interestRateRepository;
    public InterestRateController(InterestRateRepository interestRateRepository) {
        this.interestRateRepository = interestRateRepository;
    }

// Below implementation is for reading the static data from H2 database
// In same way other databases can also be used.
/*
    @GetMapping("/interest-rates-db")
    public List<InterestRate> getInterestRatesFromDB() {
        List<InterestRate> interestRates = interestRateRepository.findAll();
        return interestRates != null ? interestRates : new ArrayList<>();
    }
    @PostMapping("/save/interest-rates")
    public ResponseEntity<String> createInterestRate(@RequestBody InterestRate interestRate) {
        interestRateRepository.save(interestRate);
        return ResponseEntity.ok("Interest rate created successfully");
    }*/
}
