package com.ing.mortgage.service;

import com.ing.mortgage.model.InterestRate;
import com.ing.mortgage.model.MortgageCheckRequest;

import java.math.BigDecimal;
import java.util.List;

public interface MortgageRateService {
     List<InterestRate> getCurrentInterestRates();
     boolean isFeasible(MortgageCheckRequest request);
     BigDecimal calculateMonthlyCosts(MortgageCheckRequest request);
    }

