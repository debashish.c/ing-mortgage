package com.ing.mortgage.service;

import com.ing.mortgage.model.InterestRate;
import com.ing.mortgage.model.MortgageCheckRequest;
import com.ing.mortgage.repository.InterestRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
@Service
public class MortgageRateServiceImpl implements  MortgageRateService{
    private List<InterestRate> interestRates;
    public MortgageRateServiceImpl() {

        initializeMortgageRates();
    }

    public List<InterestRate> getCurrentInterestRates() {

        return interestRates;
    }

    private void initializeMortgageRates() {
        interestRates = new ArrayList<>();
        interestRates.add(new InterestRate(5, new BigDecimal("2.5"), LocalDateTime.now()));
        interestRates.add(new InterestRate(10, new BigDecimal("3.0"), LocalDateTime.now()));
        interestRates.add(new InterestRate(15, new BigDecimal("3.5"), LocalDateTime.now()));
    }
    public  boolean isFeasible(MortgageCheckRequest request){
        BigDecimal maxLoanValue = request.getIncome().multiply(new BigDecimal("4"));
        BigDecimal homeValue = request.getHomeValue();
        return request.getLoanValue().compareTo(maxLoanValue) <= 0 && request.getLoanValue().compareTo(homeValue) <= 0;
    }

    public BigDecimal calculateMonthlyCosts(MortgageCheckRequest request){

        // Convert the interest rate from percentage to decimal
        BigDecimal decimalInterestRate = request.getInterestRate().divide(BigDecimal.valueOf(100));

        // Calculate the monthly interest rate
        BigDecimal monthlyInterestRate = decimalInterestRate.divide(BigDecimal.valueOf(12), 8, RoundingMode.HALF_UP);

        // Calculate the number of payments (months)
        int numberOfPayments = request.getMaturityPeriod() * 12;

        // Calculate the numerator: (1 + r)^n
        BigDecimal numerator = BigDecimal.ONE.add(monthlyInterestRate).pow(numberOfPayments);

        // Calculate the denominator: (1 + r)^n - 1
        BigDecimal denominator = numerator.subtract(BigDecimal.ONE);

        // Calculate the monthly payment amount
        return request.getLoanValue()
                .multiply(monthlyInterestRate)
                .multiply(numerator)
                .divide(denominator, 2, RoundingMode.HALF_UP);

        }
}
